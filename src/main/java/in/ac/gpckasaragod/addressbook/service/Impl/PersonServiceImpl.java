/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.addressbook.service.Impl;

import in.ac.gpckasaragod.addressbook.service.PersonService;
import in.ac.gpckasaragod.addressbook.ui.model.data.Person;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author student
 */
public abstract class PersonServiceImpl extends ConnectionServiceImpl implements PersonService{
    
  
  public String savePerson(Integer id,String name,String address,String phoneNo,String email) {
        try{  
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSRET INTO PERSON (ID,NAME,ADDRESS,PHONE_NO,EMAIL,) VALUES "
                    + "('"+id+"','"+name+"','"+address+"','"+phoneNo+"','"+email+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!= 1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
           return "save failed";
        }
    }
  
  
  
  
  
  
    
  @Override
    public Person readPerson(Integer id){
        Person person = null;
         try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PERSON WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                
                String name = resultSet.getString("NAME");
                String address = resultSet.getString("ADDRESS");
                String phoneNo = resultSet.getString("PHONE_NO");
                String email = resultSet.getString("EMAIL");
                person = new Person(id,name,address,phoneNo,email);
                
            }
      } catch (SQLException ex) {
          Logger.getLogger(PersonServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      } 
      return person;
  






   }
   
   
    @Override
    public List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        try {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM PERSON";
        ResultSet resultSet = statement.executeQuery(query);
        
         while(resultSet.next()){
             int id=resultSet.getInt("ID");
             String name = resultSet.getString("NAME");
             String address = resultSet.getString("ADDRESS");
             String phoneNo = resultSet.getString("PHONENO");
             String email = resultSet.getString("EMAIL");
             Person person = new Person(id,name,address,phoneNo,email);
             persons.add(person);
          } 
    }    catch(SQLException ex) {
            Logger.getLogger(PersonServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
    }
           return persons;
    }
    
   
  @Override
    public String updatePerson(Integer id,String name,String address,String phoneNo,String email) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE PERSON SET NAME='"+name+"',ADDRESS='"+address+"',PHONE_NO='"+phoneNo+"',EMAIL='"+email+"'";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Updated successfully";
            }catch(SQLException ex){
                return "Update failed";
            }
        }
    
        
  @Override
        public String deletePerson(Integer id) {
            try{
            Connection connection = getConnection();
            String query = "DELETE FROM PERSON WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
            } catch (SQLException ex) {
                ex.printStackTrace();
                return "Delete failed";
            }
      }
}           
        
        
        
        
        
            
        
    

                        
                    
    
    
           
        
    
    
    

       