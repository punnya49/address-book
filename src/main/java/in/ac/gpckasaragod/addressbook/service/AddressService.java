/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.addressbook.service;

import in.ac.gpckasaragod.addressbook.ui.model.data.Address;
import java.util.List;



/**
 *
 * @author student
 */
public interface AddressService {
    public String saveAddress(Integer id, String addressLine1, String addressLine2, String city, String state, Integer pincode);
    public Address readAddress(Integer id);
    public List<Address> getAllAddresses();
    public String updateAddress(Integer id,String addressLine1,String addressLine2,String city,String state,Integer pincode);
    public String deleteAddress(Integer id);
}
