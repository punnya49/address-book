/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.addressbook.service;

import in.ac.gpckasaragod.addressbook.ui.model.data.Person;
import java.util.List;


/**
 *
 * @author student
 */
public interface PersonService {
    public String savePerson(String name,String address,String phone_no,String email);
    public Person readPerson(Integer id);
    public List<Person> getAllPersons();
    public String updatePerson(Integer id,String name,String address,String phone_no,String email);
    
  public String deletePerson(Integer id);
    
}  
    

