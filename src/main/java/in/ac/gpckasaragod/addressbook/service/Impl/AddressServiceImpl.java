/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.addressbook.service.Impl;

import in.ac.gpckasaragod.addressbook.service.AddressService;
import in.ac.gpckasaragod.addressbook.ui.model.data.Address;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public abstract class AddressServiceImpl extends ConnectionServiceImpl implements AddressService {
    @Override
    public String saveAddress(Integer id,String addressLine1,String addressLine2,String city,String state,Integer pincode) {
        try{  
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSRET INTO ADDRESS (ID,ADDRESS_LINE1,ADDRESS_LINE2,CITY,STATE,PINCODE) VALUES "
                    +"('"+id+"','"+addressLine1+"','"+addressLine2+"','"+city+"','"+state+"','"+pincode+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "Save failed";
            }else{
                
            
                return "Saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddressServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
           return "Save failed";
    }
}
    
    
   
    
    @Override
    public Address readAddress(Integer id) {
        Address address = null;
         try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PERSON WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
               
               String addressLine1 = resultSet.getString("ADDRESS_LINE1");
               String addressLine2 = resultSet.getString("ADDRESS_LINE2");
               String city = resultSet.getString("CITY");
               String state = resultSet.getString("STATE");
               int pincode = resultSet.getInt("PINCODE");
               
                
          }
      }catch (SQLException ex){
        Logger.getLogger(AddressServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      } 
      return address;
  }
    
    
    

    
    @Override
    public List<Address> getAllAddresses() {
        List<Address> addresses = new ArrayList <>();
        try {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT *FROM ADDRESS";
        ResultSet resultSet = statement.executeQuery(query);
        
         while(resultSet.next()){
             int id = resultSet.getInt("ID");
             String addressLine1 = resultSet.getString("ADDRESS_LINE1");
             String addressLine2 = resultSet.getString("ADDRESS_LINE2");
             String city = resultSet.getString("CITY");
             String state = resultSet.getString("STATE");
             int pincode = resultSet.getInt("PINCODE");
             Address address = new Address(id,addressLine1,addressLine2,city,state,pincode);
             addresses.add(address);
          } 
        }  catch (SQLException ex) {
            Logger.getLogger(AddressServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
           return addresses;
    }
    
    
    @Override
    public String updateAddress(Integer id,String addressLine1,String addressLine2,String city,String state,Integer pincode){
       try{
       Connection connection = getConnection();
       Statement statement = connection.createStatement();
       String query = "UPDATE ADDRESS SET ADDRESS_LINE1='"+addressLine1+"',ADDRESS_LINE2='"+addressLine2+"',CITY='"+city+"',STATE='"+state+"',PINCODE='"+pincode+"' WHERE ID="+id;
       System.out.print(query);
       int update = statement.executeUpdate(query);
       if(update !=1)
           return "Update failed";
       else
           return "Update successfully";
       }catch(SQLException ex){
           return "Update failed";
       }
    }

    
    @Override
    public String deleteAddress(Integer id) {
       try{
            Connection connection = getConnection();
            String query = "DELETE FROM ADDRESS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
        }catch (SQLException ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
    }
}    
            
  
    
    
    

